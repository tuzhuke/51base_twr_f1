#ifndef BPHERO_UWB_H
#define BPHERO_UWB_H

#include "frame_header.h"
#include "common_header.h"

#ifndef SHORT_ADDR
#define SHORT_ADDR 0x0033
#endif
extern int psduLength ;
extern srd_msg_dsss msg_f_send ; // ranging message frame with 16-bit addresses

#ifndef SPEED_OF_LIGHT
#define SPEED_OF_LIGHT      (299702547.0)     // in m/s in air
#endif
/* Buffer to store received frame. See NOTE 1 below. */
#ifndef FRAME_LEN_MAX
	#define FRAME_LEN_MAX 127
#endif

#ifndef TX_ANT_DLY
#define TX_ANT_DLY 0
#endif

#ifndef RX_ANT_DLY
#define RX_ANT_DLY 32950
#endif


#define FINAL_MSG_POLL_TX_TS_IDX 2
#define FINAL_MSG_RESP_RX_TS_IDX 6
#define FINAL_MSG_FINAL_TX_TS_IDX 10
//#define FINAL_MSG_TS_LEN 4
//#define LOCATION_FLAG_IDX 11
//#define LOCATION_INFO_LEN_IDX 12
//#define LOCATION_INFO_START_IDX 13
//#define ANGLE_MSG_MAX_LEN 30
#define FINAL_MSG_TS_LEN 4

/* UWB microsecond (uus) to device time unit (dtu, around 15.65 ps) conversion factor.
 * 1 uus = 512 / 499.2 ? and 1 ? = 499.2 * 128 dtu. */
#define UUS_TO_DWT_TIME 65536

/* Delay between frames, in UWB microseconds. See NOTE 4 below. */
/* This is the delay from Frame RX timestamp to TX reply timestamp used for calculating/setting the DW1000's delayed TX function. This includes the
 * frame length of approximately 2.46 ms with above configuration. */
#define POLL_RX_TO_RESP_TX_DLY_UUS 2600
/* This is the delay from the end of the frame transmission to the enable of the receiver, as programmed for the DW1000's wait for response feature. */
#define RESP_TX_TO_FINAL_RX_DLY_UUS 500
/* Receive final timeout. See NOTE 5 below. */
#define FINAL_RX_TIMEOUT_UUS 3300

/* Delay between frames, in UWB microseconds. See NOTE 4 below. */
/* This is the delay from the end of the frame transmission to the enable of the receiver, as programmed for the DW1000's wait for response feature. */
#define POLL_TX_TO_RESP_RX_DLY_UUS 150
/* This is the delay from Frame RX timestamp to TX reply timestamp used for calculating/setting the DW1000's delayed TX function. This includes the
 * frame length of approximately 2.66 ms with above configuration. */
#define RESP_RX_TO_FINAL_TX_DLY_UUS 3000 //2700 will fail
/* Receive response timeout. See NOTE 5 below. */
#define RESP_RX_TIMEOUT_UUS 2700




extern uint8 rx_buffer[FRAME_LEN_MAX];
/* Hold copy of status register state here for reference, so reader can examine it at a breakpoint. */
/* Hold copy of frame length of frame received (if good), so reader can examine it at a breakpoint. */
extern uint16 frame_len ;
extern void BPhero_UWB_Message_Init(void);
extern void BPhero_UWB_Init(void);//dwm1000 init related
extern float dwGetReceivePower(void);

#endif
